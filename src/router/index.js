import { createRouter, createWebHashHistory } from 'vue-router'
import Accueil from '../views/Accueil.vue'
import Bouquet from '../views/Bouquet.vue'

const routes = [
	{
		path: '/',
		name: 'Accueil',
		component: Accueil
	},
	{
		path: '/b/:id',
		name: 'Bouquet',
		component: Bouquet
	}
]

const router = createRouter({
	history: createWebHashHistory(),
	routes
})

export default router
