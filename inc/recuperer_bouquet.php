<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	if (isset($_SESSION['digibunch'][$id]['reponse'])) {
		$reponse = $_SESSION['digibunch'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT * FROM digibunch_bouquets WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		if ($bouquet = $stmt->fetchAll()) {
			$admin = false;
			if (count($bouquet, COUNT_NORMAL) > 0 && $bouquet[0]['reponse'] === $reponse) {
				$admin = true;
			}
			$donnees = $bouquet[0]['donnees'];
			if ($donnees !== '') {
				$donnees = json_decode($donnees);
			}
			$digidrive = 0;
			if (isset($_SESSION['digibunch'][$id]['digidrive'])) {
				$digidrive = $_SESSION['digibunch'][$id]['digidrive'];
			} else if (intval($bouquet[0]['digidrive']) === 1) {
				$digidrive = 1;
			}
			$date = date('Y-m-d H:i:s');
			$vues = 0;
			if ($bouquet[0]['vues'] !== '') {
				$vues = intval($bouquet[0]['vues']);
			}
			if ($admin === false) {
				$vues = $vues + 1;
			}
			$stmt = $db->prepare('UPDATE digibunch_bouquets SET vues = :vues, derniere_visite = :derniere_visite WHERE url = :url');
			if ($stmt->execute(array('vues' => $vues, 'derniere_visite' => $date, 'url' => $id))) {
				echo json_encode(array('nom' => $bouquet[0]['nom'], 'donnees' => $donnees, 'vues' => $vues, 'admin' =>  $admin, 'digidrive' => $digidrive));
			} else {
				echo 'erreur';
			}
		} else {
			echo 'contenu_inexistant';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
